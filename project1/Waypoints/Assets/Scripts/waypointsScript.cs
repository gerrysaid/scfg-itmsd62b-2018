﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointsScript : MonoBehaviour {

	public List<Vector3> waypoints;

	GameObject mycursor,mywaypoint;
	// Use this for initialization
	void Start () {
		waypoints = new List<Vector3>();

		mycursor = Resources.Load<GameObject>("Prefabs/Cursor");

		mycursor = Instantiate(mycursor,new Vector3(0f,0f),Quaternion.identity);

		mycursor.name="cursor";

		mywaypoint = Resources.Load<GameObject>("Prefabs/Waypoint");
		
	}
	
	Vector3 cursorPosition;
	// Update is called once per frame
	void Update () {

		Debug.Log(Input.mousePosition);

		if (mouseControl){
			cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			mycursor.transform.position = new Vector3(cursorPosition.x,cursorPosition.y);
		}

		if (Input.GetMouseButtonDown(0))
		{
			waypoints.Add(mycursor.transform.position);
			//create a new waypoint where I click
			mywaypoint = Instantiate(mywaypoint,mycursor.transform.position,Quaternion.identity);
			//update the number in Index
			mywaypoint.GetComponentInChildren<TextMesh>().text = waypoints.Count.ToString();

			mywaypoint.name = waypoints.Count.ToString();
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			StartCoroutine(goBack());
		}

		if (Input.GetKeyDown(KeyCode.R))
		{
			resetPath();
		}

		
	}

	void resetPath()
	{
		waypoints = new List<Vector3>();
		foreach (GameObject point in GameObject.FindGameObjectsWithTag("waypt"))
		{
			Destroy(point);
		}
	}


	bool mouseControl = true;

	IEnumerator goBack()
	{
		mouseControl = false;
		foreach(Vector3 waypoint in waypoints)
		{
			//1 second for me to go from one waypoint to the other
			float totalTime = 1f;
			float elapsedTime = 0f;
			Vector3 startpoint = mycursor.transform.position;

			while (elapsedTime<totalTime){
				mycursor.transform.position = Vector3.Lerp(startpoint,waypoint,elapsedTime/totalTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}
		mouseControl = true;
		yield return null;
	}
}
