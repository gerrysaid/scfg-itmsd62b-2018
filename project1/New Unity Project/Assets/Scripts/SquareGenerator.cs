﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareGenerator : MonoBehaviour {

	// Use this for initialization

	GameObject[] mysquares = new GameObject[3];

	GameObject[,] task1array = new GameObject[4,4];
	GameObject squarePrefab;

	GameObject squareParent;

	public List<Vector3> breadcrumbs;

	emptySquareController emptyBoxController;

	void Start () {
		//load the prefab into memory
		breadcrumbs = new List<Vector3>();

		squarePrefab = Resources.Load<GameObject>("Square2");

		//load the controller 
		

		squareParent = GameObject.Find("EmptySquare");

		task1();

		squareParent.AddComponent<emptySquareController>();

		//threeSteps();
		emptyBoxController = squareParent.GetComponent<emptySquareController>();
		emptyBoxController.parentscript = this;

	}

	void task1()
	{
		for (int rows = 0;rows<4;rows++)
		{
			for(int columns =0;columns<4;columns++)
			{
				if ((rows == 0) || (rows==3)) {
					task1array[rows,columns] = 
							Instantiate(
								squarePrefab,
								new Vector3(columns,rows),
								Quaternion.identity
							);
						
					task1array[rows,columns].GetComponent<SpriteRenderer>().color = Random.ColorHSV();
					//set the name of each box
					task1array[rows,columns].name = "Box "+rows+"-"+columns;
					//set the parent object of each box
					task1array[rows,columns].transform.parent = squareParent.transform;
				}
				else if ((columns==0) || (columns==3)){
					
					task1array[rows,columns] = 
							Instantiate(
								squarePrefab,
								new Vector3(columns,rows),
								Quaternion.identity
							);
					task1array[rows,columns].GetComponent<SpriteRenderer>().color = Random.ColorHSV();
					//set the name of each box
					task1array[rows,columns].name = "Box "+rows+"-"+columns;
					//set the parent object of each box
					task1array[rows,columns].transform.parent = squareParent.transform;
				}
			
				}
			}
		}
	


	void threeSteps(){
		

		for(int i=0;i<3;i++)
		{
			mysquares[i] = Instantiate(
				squarePrefab,
				new Vector3(i,i),
				Quaternion.identity
			);

			//set the color of each square to a random color
			mysquares[i].GetComponent<SpriteRenderer>().color = 
			Random.ColorHSV();

			mysquares[i].transform.parent = this.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//get the instance of the script attached to squareparent, and flip the boolean value of smooth inside emptysquarecontroller
			
			emptyBoxController.smooth = !emptyBoxController.smooth;
		}
		
	}
}
