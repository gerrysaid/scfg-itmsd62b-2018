﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class emptySquareController : MonoBehaviour {

	// Use this for initialization

	public bool smooth=false;

	//square generator parent script, which is going to contain the breadcrumbs
	public SquareGenerator parentscript;


	GameObject breadcrumb;

	bool goback=false;


	void Start () {
		StartCoroutine(gobackhome());

		breadcrumb = Resources.Load<GameObject>("Trail");	
	}
	
	IEnumerator gobackhome()
	{
		
		while(true)
		{
			if (goback)
			{
		
				for(int index = parentscript.breadcrumbs.Count-1;index>=0;index--)
				{
					transform.position = parentscript.breadcrumbs[index];
					//next lesson we are going to use LERP here
					yield return new WaitForSeconds(1f);
				}
				goback = false;
				yield return null;
			}else{yield return null;}
		}

	}

	void drawTrail(){
		int counter =0;
		foreach(GameObject g in GameObject.FindGameObjectsWithTag("mybreadcrumbs"))
		{
			Destroy(g);
		}
		foreach(Vector3 p in parentscript.breadcrumbs){
			GameObject crumb = Instantiate(breadcrumb,p,Quaternion.identity);
			crumb.GetComponentInChildren<TextMesh>().text = counter.ToString();
			counter++;
		}
		
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.L))
		{
			goback = !goback;
		}
		if (smooth){
			//the usual code for smooth keyboard movement
			transform.Translate(Vector3.up * Input.GetAxis("Vertical")*35f*Time.deltaTime);
			transform.Translate(Vector3.right * Input.GetAxis("Horizontal")*35f*Time.deltaTime);
		}else {
			//move one unit up every time I press the up arrow
			if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				//save the past position
				parentscript.breadcrumbs.Add(transform.position);
				transform.position += new Vector3(0f,1f);
				drawTrail();
				
			}
			//move one unit down every time I press the down arrow
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				//save the past position
				parentscript.breadcrumbs.Add(transform.position);
				transform.position -= new Vector3(0f,1f);
				drawTrail();
			}
			//move one unit right every time I press the right arrow
			if (Input.GetKeyDown(KeyCode.RightArrow))
			{
				//save the past position
				parentscript.breadcrumbs.Add(transform.position);
				transform.position += new Vector3(1f,0f);
				drawTrail();
			}
			//move one unit left every time I press the left arrow
			if (Input.GetKeyDown(KeyCode.LeftArrow))
			{
				//save the past position
				parentscript.breadcrumbs.Add(transform.position);
				transform.position -= new Vector3(1f,0f);
				drawTrail();
			}
		}

		
	}
}
