﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class squareScript : MonoBehaviour {

	//an array with five locations
	Vector3[] mycoordinates = new Vector3[5];

	GameObject mysquare;

	bool animate = false;

	Color currentColor;

	// Use this for initialization
	void Start () {
		mysquare = GameObject.Find("Square");


		currentColor = mysquare.GetComponent<SpriteRenderer>().color;

		//the middle
		mycoordinates[0] = new Vector3(0f,0f);
		//top middle
		mycoordinates[1] = new Vector3(0f,Camera.main.orthographicSize);
		//bottom middle
		mycoordinates[2] = new Vector3(0f,-Camera.main.orthographicSize);
		//right middle
		mycoordinates[3] = new Vector3(Camera.main.orthographicSize * Camera.main.aspect,0f);
		//left middle
		mycoordinates[4] = new Vector3(-Camera.main.orthographicSize * Camera.main.aspect,0f);

		//StartCoroutine(animateSquare());
		//StartCoroutine(animateSquareColors());
		StartCoroutine(stepOut());
	}


	IEnumerator stepOut()
	{
		//I am stepping out from the middle
		mysquare.transform.position = new Vector3(0f,0f);
		float poscounter = 0f;
		while (poscounter < 5f)
		{
			poscounter++;
			mysquare.transform.position = new Vector3(poscounter * Camera.main.aspect,poscounter);
			yield return new WaitForSeconds(1f);
		}
		//after the while loop is done, I can call the next coroutine
		yield return stepIn();
	}

	IEnumerator stepIn()
	{
		float poscounter = 5f;
		while (poscounter>0f)
		{
			poscounter--;
			mysquare.transform.position = new Vector3(poscounter * Camera.main.aspect,poscounter);
			yield return new WaitForSeconds(1f);
		}
		yield return null;


	}




	IEnumerator animateSquare()
	{
		int i = 0;
		//forever
		while(true)
		{
			if (animate)
			{
				//do the squares animation
				mysquare.transform.position = mycoordinates[i];
				yield return new WaitForSeconds(1f);
				if (i<5)
				{
					i++;
				}
				if (i==5) {i = 0;}
				Debug.Log(i);
			}
			else{
				//just wait
				yield return null;
			}
		}

	}

	IEnumerator animateSquareColors()
	{
		while(true){
			if (changeColors)
			{
				mysquare.GetComponent<SpriteRenderer>().color = Random.ColorHSV();
				yield return new WaitForSeconds(1f);
			}
			else{
				yield return null;
			}
		}

	}


	bool toggleRed,toggleWhite,toggleRectangle = false;

	bool changeColors = false;

	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Return))
		{
			changeColors = !changeColors;
		}

		//make all of these toggle buttons
		//----------------------------------------
		if (Input.GetKeyDown(KeyCode.R))
		{
			toggleRed = !toggleRed;
			if (toggleRed)
				mysquare.GetComponent<SpriteRenderer>().color = Color.red;
			else 
				mysquare.GetComponent<SpriteRenderer>().color = Color.white;
		}

		if (Input.GetKeyDown(KeyCode.W))
		{
			toggleWhite = !toggleWhite;
			if (toggleWhite)
				mysquare.GetComponent<SpriteRenderer>().color = Color.white;
			else
				mysquare.GetComponent<SpriteRenderer>().color = Color.black;
		}

		if (Input.GetKeyDown(KeyCode.T))
		{
			//make the square a wide rectangle
			toggleRectangle = !toggleRectangle;
			if (toggleRectangle)
				mysquare.GetComponent<Transform>().localScale = new Vector3(2f,1f);
			else
				mysquare.GetComponent<Transform>().localScale = new Vector3(1f,1f);
		}
		//---------------------------------------
		
		if (Input.GetKeyDown(KeyCode.Space))
		{
			animate = !animate;
		}
		
		//Debug.Log(animate);
		//1.Get the input of the H key
		if (Input.GetKeyDown(KeyCode.H)){

			mysquare.transform.position = new Vector3(0f,4.5f);

		}

	
	}
}
