﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	GameObject map;

	// Use this for initialization
	void Start () {
		map = GameObject.Find("Map");
		StartCoroutine(rescanMap());
	}


	IEnumerator rescanMap()
	{
		while (true)
		{
			map.GetComponent<AstarPath>().Scan();
			yield return new WaitForSeconds(0.5f);
		}
	}
	
	// Update is called once per frame

	//whenever physics collisions are involved, put all movement code in FixedUpdate
	void FixedUpdate () {
		transform.Translate(
			Vector3.right *
			Input.GetAxis("Horizontal") *
			Time.deltaTime *
			10f
		);

		transform.Translate(
			Vector3.up *
			Input.GetAxis("Vertical") *
			Time.deltaTime *
			10f
		);
	}
}
