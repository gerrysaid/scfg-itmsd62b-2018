﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circleController : MonoBehaviour {

	GameObject objectIHit;

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D otherObject)
	{
		GetComponent<SpriteRenderer>().color = Color.red;
		otherObject.gameObject.
		GetComponent<SpriteRenderer>().color = Color.red;
		objectIHit = otherObject.gameObject;
	}

	void OnTriggerExit2D(Collider2D otherObject)
	{
		GetComponent<SpriteRenderer>().color = Color.black;
		otherObject.gameObject.
		GetComponent<SpriteRenderer>().color = Color.black;
	}
	
	// Update is called once per frame
	void Update () {

		if (objectIHit!=null)
		{
			//what is the distance between the box and the circle
			GameObject.Find("CollisionManager").GetComponent<TextMesh>().text = 
			Vector3.Distance(transform.position,objectIHit.transform.position).ToString();
			//can you update this to draw a line between the box and the circle after I hit it?

			//	
		}
		


		//movements x,y
		transform.Translate(
			Vector3.up *
			Input.GetAxis("Vertical") * 15f *
			Time.deltaTime
		);
		//------------
		transform.Translate(
			Vector3.right *
			Input.GetAxis("Horizontal") * 15f *
			Time.deltaTime
		);
	}
}
